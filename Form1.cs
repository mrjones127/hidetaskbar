﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HideTaskbar
{
    public partial class Form1 : Form
    {
        private Size originalSize;
        public Form1()
        {
            InitializeComponent();
            
        }

        [DllImport("user32.dll")]
        private static extern int FindWindow(string className, string windowText);

        [DllImport("user32.dll")]
        private static extern int ShowWindow(int hwnd, int command);

        [DllImport("user32.dll")]
        private static extern IntPtr FindWindowEx(
            IntPtr parentHwnd,
            IntPtr childAfterHwnd,
            IntPtr className,
            string windowText);

        
        private const int SW_HIDE = 0;
        private const int SW_SHOW = 1;
        private bool active;

        private void TaskbarHdie_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            int hwndOrb = (int)FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)0xC017, null);
            int hwnd = FindWindow("Shell_TrayWnd", "");
            ShowWindow(hwnd, SW_SHOW);
            ShowWindow(hwndOrb, SW_SHOW);
            Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Show();
            Opacity = 0.5;
            Size = originalSize;
            ShowInTaskbar = true;
            Location = new Point(SystemInformation.PrimaryMonitorSize.Width / 2 - Width / 2, SystemInformation.PrimaryMonitorSize.Height - Height / 2);
            int hwndOrb = (int)FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)0xC017, null);
            int hwnd = FindWindow("Shell_TrayWnd", "");
            ShowWindow(hwnd, SW_HIDE);
            ShowWindow(hwndOrb, SW_HIDE);
            return;
            ShowInTaskbar = false;
            originalSize = Size;
            Hide();
            Size = new Size(0, 0);
        }

        private void TaskbarHdie_MouseClick(object sender, MouseEventArgs e)
        {
            Show();
            Opacity = 0.5;
            Size = originalSize;
            ShowInTaskbar = true;
            Location = new Point(SystemInformation.PrimaryMonitorSize.Width / 2 - Width / 2, SystemInformation.PrimaryMonitorSize.Height - Height / 2);
            int hwndOrb = (int)FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)0xC017, null);
            int hwnd = FindWindow("Shell_TrayWnd", "");
            ShowWindow(hwnd, SW_HIDE);
            ShowWindow(hwndOrb, SW_HIDE);
        }
    }
}